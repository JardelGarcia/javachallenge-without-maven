JardelJavaChallenge

- How to compile and run the application.

1 - Import the web project on the IDE or deploy it within the server
2 - Start Apache TomCat server.
3 - Put the url "http://localhost:8080/JardelJavaChallenge/rest/ + @Path + method and parameters"
	For example:
		To list all products:
		URL = http://localhost:8080/JardelJavaChallenge/rest/product/listAll

		To find a product by id:
		URL = http://localhost:8080/JardelJavaChallenge/rest/product/get/(id)


4 - The funcions developed were:
	Run the webservice (using Jersey, JSON, Jackson, etc)
	List all products and orders
	List products and orders by id
	Create order
	Delele order


Obs.:

	- Some of the technologies required in the scope of the test were not used (like Guava
slf4j/logback, H2, Mockito, PowerMock, dbUnit, etc) due to the little time I had to complete the test. I had to study some of them and tried to understand and include all I could in this short time. The webservice I created uses some technologies I already used. 


Thank you for all the attention and knowledge I could have in this short time!

- Jardel Garcia

