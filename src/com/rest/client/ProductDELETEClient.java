package com.rest.client;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class ProductDELETEClient {

	public static void main(String[] args) {
		Client client = Client.create();
		WebResource webResource = client.resource("http://localhost:8080/JardelJavaChallenge/rest/product/delete/100");
		ClientResponse response = webResource.type("application/xml").delete(ClientResponse.class);
		System.out.println("Delete operation response processing...\n");
		String output = response.getEntity(String.class);
		System.out.println(output);
	}
}
