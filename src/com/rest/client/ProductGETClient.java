package com.rest.client;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class ProductGETClient {

	public static void main(String[] args) {
		Client client = new Client();
		WebResource webResource = client.resource("http://localhost:8080/JardelJavaChallenge/rest/product/get/100");
		ClientResponse response = webResource.type("application/xml").get(ClientResponse.class);
		System.out.println("Get operation response processing...\n");
		String output = response.getEntity(String.class);
		System.out.println(output);
	}

}
