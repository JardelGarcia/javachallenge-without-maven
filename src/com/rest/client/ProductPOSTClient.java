package com.rest.client;

import com.rest.product.Product;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class ProductPOSTClient {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Client client = Client.create();
		WebResource webResource = client.resource("http://localhost:8080/JardelJavaChallenge/rest/product/create");
		
		Product product = new Product();
		product.setId(2);
		product.setName("Test 2");
		product.setPrice(200.0);
		
		ClientResponse response = webResource.type("application/xml").post(ClientResponse.class, product);
		System.out.println("Create operation response processing...\n");
		String output = response.getEntity(String.class);
		System.out.println(output);

	}

}
