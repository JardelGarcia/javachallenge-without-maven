package com.rest.client;

import com.rest.product.Product;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class ProductPUTClient {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Client client = Client.create();
		WebResource webResource = client.resource("http://localhost:8080/JardelJavaChallenge/rest/product/update");
		
		Product product = new Product();
		product.setId(3);
		product.setName("Test 3");
		product.setPrice(300.0);
		
		ClientResponse response = webResource.type("application/xml").put(ClientResponse.class, product);
		System.out.println("Update operation response processing...\n");
		String output = response.getEntity(String.class);
		System.out.println(output);

	}

}
