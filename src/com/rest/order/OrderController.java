package com.rest.order;

import java.util.ArrayList;

public class OrderController {

	public ArrayList<Orders> listAll(){
		System.out.println("Listing all orders");
		return OrderDAO.getInstance().listAll();
	}
	
	public Orders findById(Integer id){
		return OrderDAO.getInstance().findById(id);
	}
}
