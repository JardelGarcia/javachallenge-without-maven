package com.rest.order;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import com.rest.factory.ConnectionFactory;

public class OrderDAO extends ConnectionFactory {

	static OrderDAO instance;

	public static OrderDAO getInstance() {
		if (instance == null)
			instance = new OrderDAO();
		return instance;
	}

	public Orders insertProduct(String name) {
		Connection connection = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Orders order = null;

		connection = openConnection();

		try {
			pstmt = connection.prepareStatement("INSERT INTO orders VALUES ("
					+ name + ", " + 0.0);
			pstmt.execute();

			pstmt = connection.prepareStatement("SELECT MAX FROM orders");
			rs = pstmt.executeQuery();
			
			while (rs.next()) {
				order = new Orders();
				order.setId(rs.getInt("id"));
				order.setProduct(rs.getInt("product"));
				order.setClientName(rs.getString("clientName"));
				order.setClientAdress(rs.getString("clientAdress"));
			}

		} catch (Exception e) {
			System.out.println("Error: could not list products: " + e);
			e.printStackTrace();
		} finally {
			closeConnection(connection, pstmt, rs);
		}
		return order;
	}

	public void delete(Integer id) {
		Connection connection = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		connection = openConnection();

		try {
			pstmt = connection
					.prepareStatement("delete from orders where id = " + id);
			pstmt.execute();
		} catch (Exception e) {
			System.out.println("Error: could not list orders: " + e);
			e.printStackTrace();
		} finally {
			closeConnection(connection, pstmt, rs);
		}
	}
	
	public Orders findById(Integer id) {
		Connection connection = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Orders order = null;

		connection = openConnection();

		try {
			pstmt = connection
					.prepareStatement("select * from orders where id = " + id);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				order = new Orders();
				order.setId(rs.getInt("id"));
				order.setProduct(rs.getInt("product"));
				order.setClientName(rs.getString("clientName"));
				order.setClientAdress(rs.getString("clientAdress"));
			}

		} catch (Exception e) {
			System.out.println("Error: could not list orders: " + e);
			e.printStackTrace();
		} finally {
			closeConnection(connection, pstmt, rs);
		}
		return order;
	}

	public ArrayList<Orders> listAll() {
		Connection connection = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ArrayList<Orders> orders = null;

		connection = openConnection();
		orders = new ArrayList<Orders>();
		try {
			pstmt = connection
					.prepareStatement("select * from orders");
			rs = pstmt.executeQuery();

			while (rs.next()) {
				Orders order = new Orders();

				order.setId(rs.getInt("id"));
				order.setProduct(rs.getInt("product"));
				order.setClientName(rs.getString("clientName"));
				order.setClientAdress(rs.getString("clientAdress"));
				orders.add(order);
			}

		} catch (Exception e) {
			System.out.println("Error: could not list products: " + e);
			e.printStackTrace();
		} finally {
			closeConnection(connection, pstmt, rs);
		}
		return orders;
	}

}
