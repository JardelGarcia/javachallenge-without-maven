package com.rest.order;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name="orders")
public class Orders {

	private Integer id;
	private Integer product;
	private String clientName;
	private String clientAdress;
	
	@XmlElement(required=true)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getProduct() {
		return product;
	}
	public void setProduct(Integer product) {
		this.product = product;
	}
	public String getClientName() {
		return clientName;
	}
	public void setClientName(String clientName) {
		this.clientName = clientName;
	}
	public String getClientAdress() {
		return clientAdress;
	}
	public void setClientAdress(String clientAdress) {
		this.clientAdress = clientAdress;
	}
	
	@Override
	public String toString() {
		return "Order: " + id + " - Product: " + product + " - Client: " + clientName + " - Adress: " + clientAdress; 
	}
}
