package com.rest.product;

import java.util.ArrayList;

public class ProductController {

	public ArrayList<Product> listAll(){
		System.out.println("Listing all products");
		return ProductDAO.getInstance().listAll();
	}
	
	public Product findById(Integer id){
		return ProductDAO.getInstance().findById(id);
	}
}
