package com.rest.product;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import com.rest.factory.ConnectionFactory;

public class ProductDAO extends ConnectionFactory {

	static ProductDAO instance;

	public static ProductDAO getInstance() {
		if (instance == null)
			instance = new ProductDAO();
		return instance;
	}

	public Product insertProduct(String name) {
		Connection connection = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Product product = null;

		connection = openConnection();

		try {
			pstmt = connection.prepareStatement("INSERT INTO product VALUES ("
					+ name + ", " + 0.0);
			pstmt.execute();

			pstmt = connection.prepareStatement("SELECT MAX FROM product");
			rs = pstmt.executeQuery();
			
			while (rs.next()) {
				product = new Product();
				product.setId(rs.getInt("id"));
				product.setName(rs.getString("name"));
				product.setPrice(rs.getDouble("price"));
			}

		} catch (Exception e) {
			System.out.println("Error: could not list products: " + e);
			e.printStackTrace();
		} finally {
			closeConnection(connection, pstmt, rs);
		}
		return product;
	}

	public Product findById(Integer id) {
		Connection connection = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Product product = null;

		connection = openConnection();

		try {
			pstmt = connection
					.prepareStatement("select * from product where id = " + id);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				product = new Product();
				product.setId(rs.getInt("id"));
				product.setName(rs.getString("name"));
				product.setPrice(rs.getDouble("price"));
			}

		} catch (Exception e) {
			System.out.println("Error: could not list products: " + e);
			e.printStackTrace();
		} finally {
			closeConnection(connection, pstmt, rs);
		}
		return product;
	}

	public ArrayList<Product> listAll() {
		Connection connection = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ArrayList<Product> products = null;

		connection = openConnection();
		products = new ArrayList<Product>();
		try {
			pstmt = connection
					.prepareStatement("select * from product order by name");
			rs = pstmt.executeQuery();

			while (rs.next()) {
				Product product = new Product();

				product.setId(rs.getInt("id"));
				product.setName(rs.getString("name"));
				product.setPrice(rs.getDouble("price"));
				products.add(product);
			}

		} catch (Exception e) {
			System.out.println("Error: could not list products: " + e);
			e.printStackTrace();
		} finally {
			closeConnection(connection, pstmt, rs);
		}
		return products;
	}

}
