package com.rest.resource;

import java.net.URISyntaxException;
import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.rest.order.Orders;
import com.rest.order.OrderController;
import com.rest.order.OrderDAO;
import com.rest.product.Product;

@Path("/orders")
public class OrderService {

	@GET
	@Path("/listAll")
	@Produces("application/json")
	public ArrayList<Orders> listAll(){
		return new OrderController().listAll();
	}
	
	@GET
	@Path("/get/{id}")
	@Produces("application/json")
	public Orders getProduct(@PathParam("id") Integer id){
		return OrderDAO.getInstance().findById(id);
	}
	
	@POST
	@Path("/insert/{name}")
	@Produces("application/json")
	public Orders insertProduct(@PathParam("name") String name){
		return OrderDAO.getInstance().insertProduct(name);
	}
	
	@PUT
	@Path("/update")
	@Consumes(MediaType.APPLICATION_XML)
	@Produces(MediaType.APPLICATION_XML)
	public Product updateProduct(Product product){
		product.setName(product.getName() + " updated");
		return product;
	}
	
	@DELETE
	@Path("/delete/{id}")
	public Response deleteProduct(@PathParam("id") int id) throws URISyntaxException {
		OrderDAO.getInstance().delete(id);
		return Response.status(200).entity(" Product " + id + " deleted successfully").build();
	}
	
}
