package com.rest.resource;

import java.net.URISyntaxException;
import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.rest.product.Product;
import com.rest.product.ProductController;
import com.rest.product.ProductDAO;

@Path("/product")
public class ProductService {

	@GET
	@Path("/listAll")
	@Produces("application/json")
	public ArrayList<Product> listarTodos(){
		return new ProductController().listAll();
	}
	
	@GET
	@Path("/get/{id}")
	@Produces("application/json")
	public Product getProduct(@PathParam("id") Integer id){
		return ProductDAO.getInstance().findById(id);
	}
	
	@POST
	@Path("/insert/{name}")
	@Produces("application/json")
	public Product insertProduct(@PathParam("name") String name){
		return ProductDAO.getInstance().insertProduct(name);
	}
	
	@PUT
	@Path("/update")
	@Consumes(MediaType.APPLICATION_XML)
	@Produces(MediaType.APPLICATION_XML)
	public Product updateProduct(Product product){
		product.setName(product.getName() + " updated");
		return product;
	}
	
	@DELETE
	@Path("/delete/{id}")
	public Response deleteProduct(@PathParam("id") int id) throws URISyntaxException {
		return Response.status(200).entity(" Product " + id + " deleted successfully").build();
	}
	
}
